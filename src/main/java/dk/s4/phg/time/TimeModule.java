package dk.s4.phg.time;

import dk.s4.phg.baseplate.ApplicationState;
import dk.s4.phg.baseplate.Callback;
import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.baseplate.EventImplementation;
import dk.s4.phg.baseplate.FunctionImplementation;
import dk.s4.phg.baseplate.InterfaceEndpoint;
import dk.s4.phg.baseplate.ModuleBase;
import dk.s4.phg.baseplate.MetaData;

import dk.s4.phg.messages.interfaces.time.AnnounceClock;
import dk.s4.phg.messages.interfaces.time.ReadTime;
import dk.s4.phg.messages.interfaces.time.ReadTimeSuccess;
import dk.s4.phg.messages.interfaces.time.SolicitClock;
import dk.s4.phg.messages.interfaces.time.StartTimer;
import dk.s4.phg.messages.interfaces.time.StartTimerSuccess;
import dk.s4.phg.messages.interfaces.time.StopTimer;
import dk.s4.phg.messages.interfaces.time.StopTimerSuccess;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.app.Activity;

/**
 * Timer and Real-time services for the Java VM.
 *
 * This class implements the platform-specific PHG core time module
 * for the Java Virtual Machine platform.
 *
 * @author <a href="mailto:jacob.andersen@alexandra.dk">Jacob
 *         Andersen</a>, The Alexandra Institute.
 * @author <a href="mailto:jonas.lindstrom@alexandra.dk">Jonas
 *         Lindstrøm</a>, The Alexandra Institute.
 * @author Claus Christiansen, The Alexandra Institute.
 *
 * @copyright &copy; 2019 <a href="https://www.4s-online.dk">The
 *            4S Foundation</a>. Licensed under the
 *            <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache
 *            License ver. 2.0</a>.
 */
public class TimeModule extends ModuleBase {

    // Conversion factors
    private static final int NANOSECONDS_PR_MILLISECOND = 1000000;
    private static final int MILLISECONDS_PR_MINUTE = 60000;

    // The executor of timer events
    private final ScheduledThreadPoolExecutor executor;

    // Active timers with their cancellation handles
    private class ActiveTimer {
        Callback<StartTimerSuccess> callback;
        ScheduledFuture cancellationHandle;
    }
    private final Map<Integer, ActiveTimer> activeTimers = new HashMap<>();

    // The next available timerId
    private int nextTimerId = 0;

    // The Time interface endpoint
    private final InterfaceEndpoint endpoint;

    /**
     * Construct and start the Time module.
     *
     * @param context      The PHG Core common Context object shared by
     *                     all modules connected to the JVM baseplate
     * @param appActivity  The Android activity
     */
    public TimeModule(Context context, Activity appActivity) {

        // Create the base class
        super(context, "Time");

        // Create the producer interface endpoint and connect handlers
        endpoint = implementsProducerInterface("Time");

        endpoint.addFunctionHandler("ReadTime",
            new FunctionImplementation<ReadTime, ReadTimeSuccess>() {
                public void apply(ReadTime args,
                                  Callback<ReadTimeSuccess> callback,
                                  MetaData meta) {
                    try {
                        callback.success(readTime(args), false);
                    } catch (IllegalArgumentException ex) {
                        CoreError err = endpoint.createError(1, "Invalid or missing argument");
                        err.causedBy = getLogger().createError(ex);
                        callback.error(err, false);
                    }
                }
            });
        endpoint.addFunctionHandler("StartTimer",
            new FunctionImplementation<StartTimer, StartTimerSuccess>() {
                public void apply(StartTimer args,
                                  Callback<StartTimerSuccess> callback,
                                  MetaData meta) {
                    startTimer(args, callback);
                }
            });
        endpoint.addFunctionHandler("StopTimer",
            new FunctionImplementation<StopTimer, StopTimerSuccess>() {
                public void apply(StopTimer args,
                                  Callback<StopTimerSuccess> callback,
                                  MetaData meta) {
                    if (stopTimer(args)) {
                        // Timer not active
                        callback.error(endpoint.createError(1, "Invalid or missing argument: timerId not found"), false);
                    } else {
                        callback.success(StopTimerSuccess.getDefaultInstance(), false);
                    }
                }
            });
        endpoint.addEventHandler("SolicitClock",
            new EventImplementation<SolicitClock>() {
                public void accept(SolicitClock args, MetaData meta) {
                    announceClock();
                }
            });

        // Create the executor of timer events
        executor = new ScheduledThreadPoolExecutor(1);
        executor.setRemoveOnCancelPolicy(true);

        // Start the module
        start();
    }

    /**
     * Create a fresh timerId
     */
    private int createTimerId() {
        return nextTimerId++;
    }

    /**
     * Announce the clock when the application starts to avoid
     * unnecessary announcements, and stop all future timer events
     * when shutting down.
     */
    @Override
    protected void onApplicationStateChange(ApplicationState newState) {
        if (newState == ApplicationState.RUNNING) {
            announceClock();
        } else if (newState == ApplicationState.FINALIZING) {
            executor.shutdownNow();
        }
    }

    /**
     * Announce myself
     */
    private void announceClock() {
        endpoint.postEvent("AnnounceClock", AnnounceClock.getDefaultInstance(), null);
    }

    /**
     * Handle the "read time" function call: Read the real-time clock.
     * @param request The request arguments
     * @return The response matching the request
     * @throws IllegalArgumentException If anything is wrong with the
     *                                  arguments
     */
    protected ReadTimeSuccess readTime(ReadTime request) {
        // The ZZZZZ format will format UTC as 2019-01-31T12:34:56.987Z and
        // 'Europe/Copenhagen' as 2019-01-31T12:34:56.987+01:00

        // Use UTC or local time zone
        TimeZone zone = request.getUtc() ? TimeZone.getTimeZone("UTC") : TimeZone.getDefault();
        ReadTimeSuccess.Builder builder = ReadTimeSuccess.newBuilder().setUtc(request.getUtc());

        Date date = new Date();

        switch (request.getFormatValue()) {
        case ReadTime.Format.ISO8601EXT_VALUE:
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");
            sdf.setTimeZone(zone);
            builder.setIso8601Ext(sdf.format(date));
            break;

        case ReadTime.Format.PARTS_VALUE:
            LocalDate localDate = date.toInstant().atZone(zone.toZoneId()).toLocalDate();
            LocalTime localTime = date.toInstant().atZone(zone.toZoneId()).toLocalTime();
            builder.setParts(
                ReadTimeSuccess.TimeParts.newBuilder()
                .setYear(localDate.getYear())
                .setMonth(localDate.getMonthValue())
                .setDay(localDate.getDayOfMonth())
                .setHour(localTime.getHour())
                .setMin(localTime.getMinute())
                .setSec(localTime.getSecond())
                .setMs(localTime.getNano() / NANOSECONDS_PR_MILLISECOND)
                .setTimezone(zone.getOffset(date.getTime()) / MILLISECONDS_PR_MINUTE)
            );
            break;
        }
        return builder.build();
    }

    /**
     * Handle the "start timer" function call: Set a new timer.
     * @param request  The request arguments
     * @param response The response matching the request
     */
    protected void startTimer(StartTimer request, Callback<StartTimerSuccess> response) {
        // Duration is an unsigned 32 bit value, so we convert to a 64
        // bit int to avoid problems with large values.
        final long duration = request.getDelayMillis() & 0x0000FFFFL;
        final boolean repeat = request.getRepeat();

        if (duration == 0) {
            response.error(endpoint.createError(1, "Invalid or missing argument: delay_millis"), false);
            return;
        }

        // We will be setting a new timer with the following id
        final int timerId = createTimerId();

        // Construct task to execute
        Runnable task = () -> enqueueAction(() -> signal(timerId, repeat));

        // Set the timer
        ScheduledFuture cancellationHandle = repeat ?
            executor.scheduleAtFixedRate(task, duration, duration, TimeUnit.MILLISECONDS) :
            executor.schedule(task, duration, TimeUnit.MILLISECONDS);

        // Save the callback and cancellation handle
        ActiveTimer at = new ActiveTimer();
        at.callback = response;
        at.cancellationHandle = cancellationHandle;
        activeTimers.put(timerId, at);

        // Return the timerId to the caller
        response.success(StartTimerSuccess.newBuilder()
                         .setTimerId(timerId)
                         .setFired(false)
                         .build(), true);
    }

    /**
     * Handle a firing timer
     * @param timerId   The id of the timer that has fired.
     * @param repeating true if this is a repeating timer.
     */
    private void signal(int timerId, boolean repeating) {
        ActiveTimer at = activeTimers.get(timerId);
        if (at == null) {
            // Concurrently stopped...
            return;
        }

        // The response message to return
        StartTimerSuccess payload = StartTimerSuccess.newBuilder()
            .setTimerId(timerId)
            .setFired(true)
            .build();

        if (repeating) {
            at.callback.success(payload, false);
        } else {
            // Single-shot timers are removed when fired the first
            // time
            at.callback.success(payload, false);
            activeTimers.remove(timerId);
        }
    }

    /**
     * Handle the "stop timer" function call: Set a new timer.
     * @param request The request arguments
     * @return true If the provided timerId was not found among the
     *              active timers
     */
    protected boolean stopTimer(StopTimer request) {
        int timerId = request.getTimerId();
        ActiveTimer at = activeTimers.get(timerId);
        if (at == null) {
            // non-existent or already stopped...
            return true;
        }

        // Cancel the timer
        at.cancellationHandle.cancel(false);

        // Send a dummy message to release all callback resources.
        at.callback.success(StartTimerSuccess.newBuilder()
                            .setTimerId(timerId)
                            .setFired(false)
                            .build(), false);
        // And remove the timer from the list of active timers.
        activeTimers.remove(timerId);

        return false;
    }
}
