package dk.s4.phg.time;

import android.app.Activity;

import com.google.protobuf.MessageLite;

import org.junit.Before;
import org.junit.Test;

import dk.s4.phg.baseplate.ApplicationState;
import dk.s4.phg.baseplate.Callback;
import dk.s4.phg.baseplate.Context;
import dk.s4.phg.baseplate.CoreError;
import dk.s4.phg.messages.interfaces.time.ReadTime;
import dk.s4.phg.messages.interfaces.time.ReadTimeSuccess;
import dk.s4.phg.messages.interfaces.time.StartTimer;
import dk.s4.phg.messages.interfaces.time.StartTimerSuccess;
import dk.s4.phg.messages.interfaces.time.StopTimer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class TimeModuleTest {

    private TimeModule timeModule;

    @Before
    public void setUp() {
        timeModule = new TimeModule(new MockContext(), mock(Activity.class));

    }

    @Test
    public void isInitialized() {
        assertThat(timeModule).isNotNull();
        assertThat(timeModule.getApplicationState()).isEqualTo(ApplicationState.INITIALIZING);
    }

    @Test
    public void testReadTimePartsFormatNonUtc() {
        ReadTime readTime = ReadTime.newBuilder().setFormat(ReadTime.Format.PARTS).setUtc(false).build();

        ReadTimeSuccess readTimeSuccess = timeModule.readTime(readTime);

        assertThat(readTimeSuccess.isInitialized()).isTrue();
        assertThat(readTimeSuccess.hasParts()).isTrue();
        assertThat(readTimeSuccess.getUtc()).isFalse();
    }

    @Test
    public void testReadTimePartsFormatUtc() {
        ReadTime readTime = ReadTime.newBuilder().setFormat(ReadTime.Format.PARTS).setUtc(true).build();

        ReadTimeSuccess readTimeSuccess = timeModule.readTime(readTime);

        assertThat(readTimeSuccess.isInitialized()).isTrue();
        assertThat(readTimeSuccess.hasParts()).isTrue();
        assertThat(readTimeSuccess.getUtc()).isTrue();
    }

    @Test
    public void testReadTimeIso8601extUtcFormat() {
        ReadTime readTime = ReadTime.newBuilder().setFormat(ReadTime.Format.ISO8601EXT).setUtc(true).build();

        ReadTimeSuccess readTimeSuccess = timeModule.readTime(readTime);

        assertThat(readTimeSuccess.isInitialized()).isTrue();
        assertThat(readTimeSuccess.hasParts()).isFalse();
        assertThat(readTimeSuccess.getUtc()).isTrue();
    }

    @Test
    public void testReadTimeIso8601extNonUtcFormat() {
        ReadTime readTime = ReadTime.newBuilder().setFormat(ReadTime.Format.ISO8601EXT).setUtc(false).build();

        ReadTimeSuccess readTimeSuccess = timeModule.readTime(readTime);

        assertThat(readTimeSuccess.isInitialized()).isTrue();
        assertThat(readTimeSuccess.hasParts()).isFalse();
        assertThat(readTimeSuccess.getUtc()).isFalse();
    }

    /**
     * Expecting this to blow up!?
     */
    /*@Test
    public void testReadTimeUnrecognizedFormat() {
        ReadTime readTime = ReadTime.newBuilder().setFormatValue(-42).build();

        ReadTimeSuccess readTimeSuccess = timeModule.readTime(readTime);

        assertThat(readTimeSuccess.isInitialized()).isTrue();
    }*/
    @Test
    public void testStartTimerSetsErrorWhenDuration0() {
        TestCallback<StartTimerSuccess> callback = new TestCallback<>();
        StartTimer build = StartTimer.newBuilder().setDelayMillis(0).build();

        timeModule.startTimer(build, callback);

        assertThat(callback.getError()).isNotNull();
        assertThat(callback.getSuccess()).isNull();
        assertThat(callback.getError().message).contains("delay_millis");
    }

    @Test
    public void testStartTimerSetsSuccess() {
        TestCallback<StartTimerSuccess> callback = new TestCallback<>();
        StartTimer build = StartTimer.newBuilder().setDelayMillis(5000).build();

        timeModule.startTimer(build, callback);

        assertThat(callback.getError()).isNull();
        assertThat(callback.getSuccess()).isNotNull();
        assertThat(callback.getSuccess().getTimerId()).isEqualTo(0);
    }

    @Test
    public void testStartTimerIdIncreases() {
        TestCallback<StartTimerSuccess> callback = new TestCallback<>();
        StartTimer build = StartTimer.newBuilder().setDelayMillis(5000).build();

        timeModule.startTimer(build, callback);
        int timerId1 = callback.getSuccess().getTimerId();

        timeModule.startTimer(build, callback);
        int timerId2 = callback.getSuccess().getTimerId();

        assertThat(timerId1).isLessThan(timerId2);
    }

    @Test
    public void testStopTimerNotExisting() {
        boolean stopTimerResult = timeModule.stopTimer(StopTimer.newBuilder().build());
        assertThat(stopTimerResult).isTrue();
    }

    @Test
    public void testStopTimerExisting() {
        TestCallback<StartTimerSuccess> callback = new TestCallback<>();
        StartTimer build = StartTimer.newBuilder().setDelayMillis(5000).build();

        timeModule.startTimer(build, callback);
        int timerId = callback.getSuccess().getTimerId();

        boolean stopTimerResult = timeModule.stopTimer(StopTimer.newBuilder().setTimerId(timerId).build());
        assertThat(stopTimerResult).isFalse();
    }

 /*   @Test
    public void callEndpoints() {
        TestModule testModule = new TestModule();
        TestCallback<StartTimerSuccess> callback = new TestCallback<>();
        testModule.call("StartTimer", StartTimer.newBuilder().build(), Peer.fromInt32(42), callback);

    }*/

    class MockContext extends Context {
        MockContext() {
            super(42);
        }

        @Override
        protected void initialize() {

        }
    }

    class TestCallback<S extends MessageLite> implements Callback<S> {
        private S success;
        private CoreError error;

        @Override
        public void success(S s, boolean hasMore) {
            success = s;
        }

        @Override
        public void error(CoreError coreError, boolean hasMore) {
            error = coreError;
        }

        public CoreError getError() {
            return error;
        }

        public S getSuccess() {
            return success;
        }
    }

 /*   public class TestModule extends ModuleBase {
        private final InterfaceEndpoint endpoint;

        public TestModule() {
            super(new MockContext(), "Test");
            endpoint = implementsProducerInterface("Time");
        }

        public <A extends MessageLite, R extends MessageLite> void  call(String functionName, A args, Peer receiver, Callback<? super R> callback ) {
            endpoint.callFunction(functionName, args, receiver, callback);
        }


    }*/

}


